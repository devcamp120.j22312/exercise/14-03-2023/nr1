// Khai bao thư viện express
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

// Khai báo port
const port = 8000;

// Import Model
const userModel = require('./app/models/userModel');
const diceHistoryModel = require('./app/models/diceHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const voucherModel = require('./app/models/voucherModel');

// Import Router
const userRouter = require('./app/routes/userRouter');
const diceHistoryRouter = require('./app/routes/diceHistoryRouter');
const prizeRouter = require('./app/routes/prizeRouter');
const voucherRouter = require('./app/routes/voucherRouter');

// Khai báo app
const app = express();

app.use(express.static(__dirname + '/views'))

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

app.get('/', (request, response, next) => {
    const randomNumber = Math.floor((Math.random() * 6) + 1);
    console.log(randomNumber);
    next();
})

app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + "/views/Task 31.30.html"));
})

main().catch(err => console.log(err));
async function main() {
    await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_LuckyDice');
    console.log("Connect MongoDB Successfully");
}

// mongoose.connect("mongodb://127.0.0.1/CRUD_LuckyDice", (error) => {
//     if (error)
//         throw error;

//     console.log("Connect MongoDB Successfully!");
// })

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})