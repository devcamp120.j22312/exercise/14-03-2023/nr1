// Khai báo thu viện mongoose
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const voucherSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    code: {
        type: String,
        unique: true,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("Voucher", voucherSchema);