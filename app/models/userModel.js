// Khai báo thu viện mongoose
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        type: String,
        unique: true,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("User", userSchema);