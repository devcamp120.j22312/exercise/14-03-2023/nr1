const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const diceHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("DiceHistory", diceHistorySchema);