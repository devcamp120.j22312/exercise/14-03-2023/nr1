// Khai báo thu viện mongoose
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const prizeSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("Prize", prizeSchema);