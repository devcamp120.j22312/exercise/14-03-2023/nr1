const express = require('express');

const diceHistoryController = require('../controllers/diceHistoryController');

const router = express.Router();

router.post('/dice-histories', diceHistoryController.createDiceHistory);

router.get('/dice-histories', diceHistoryController.getAllDiceHistory);

router.get('/dice-histories/:diceHistoryId', diceHistoryController.getDiceHistoryById);

router.put('/dice-histories/:diceHistoryId', diceHistoryController.updateDiceHistoryById);

router.delete('/dice-histories/:diceHistoryId', diceHistoryController.deleteDiceHistoryById);

module.exports = router;