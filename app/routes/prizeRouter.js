const express = require('express');

const prizeController = require('../controllers/prizeController');

const router = express.Router();

router.post('/prizes', prizeController.createPrize);

router.get('/prizes', prizeController.getAllPrize);

router.get('/prizes/:prizeId', prizeController.getPrizeById);

router.put('/prizes/:prizeId', prizeController.updatePrizeById);

router.delete('/prizes/:prizeId', prizeController.deletePrizeById);

module.exports = router;