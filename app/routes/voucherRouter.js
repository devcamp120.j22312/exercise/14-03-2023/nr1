const express = require('express');

const voucherController = require('../controllers/voucherController');

const router = express.Router();

router.post('/vouchers', voucherController.createVoucher);

router.get('/vouchers', voucherController.getAllVoucher);

router.get('/vouchers/:voucherId', voucherController.getVoucherById);

router.put('/vouchers/:voucherId', voucherController.updateVoucherById);

router.delete('/vouchers/:voucherId', voucherController.deleteVoucherById);

module.exports = router;