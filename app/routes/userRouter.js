const express = require('express');

const userController = require('../controllers/userController');

const router = express.Router();

router.post('/users', userController.createUser);

router.get('/users', userController.getAllUser);

router.get('/users/:userId', userController.getUserById);

router.put('/users/:userId', userController.updateUserById);

router.delete('/users/:userId', userController.deleteUserById);

module.exports = router;