const mongoose = require('mongoose');

const prizeModel = require('../models/prizeModel');

const createPrize = (request, response) => {
    let body = request.body;

    if (!body.name) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Name is required!'
        })
    }

    let newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    prizeModel.create(newPrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'New Prize Has Been Created!',
                data: data
            })
        }
    })
}

const getAllPrize = (request, response) => {
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Prize',
                data: data
            })
        }
    })
}

const getPrizeById = (request, response) => {
    const prizeId = request.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Prize ID is not valid'
        })
    }

    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Prize By Id',
                data: data
            })
        }
    })
}

const updatePrizeById = (request, response) => {
    const prizeId = response.params.prizeId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Prize ID is not valid'
        })
    }
    if (!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Prizename is required'
        })
    }
    
    let updatePrize = {
        name: body.name,
        description: body.description,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deletePrizeById = (request, response) => {
    const prizeId = request.params.prizeId;

    //
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'PrizeId is invalid'
        })
    }
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}