const mongoose = require('mongoose');

const voucherModel = require('../models/voucherModel');

const createVoucher = (request, response) => {
    let body = request.body;

    if (!body.code) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Code is required!'
        })
    }
    if (!body.discount || body.discount < 0) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Code is required!'
        })
    }

    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'New Voucher Has Been Created!',
                data: data
            })
        }
    })
}

const getAllVoucher = (request, response) => {
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Voucher',
                data: data
            })
        }
    })
}

const getVoucherById = (request, response) => {
    const voucherId = request.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Voucher ID is not valid'
        })
    }

    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Voucher By Id',
                data: data
            })
        }
    })
}

const updateVoucherById = (request, response) => {
    const voucherId = response.params.voucherId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Voucher ID is not valid'
        })
    }
    if (!body.code) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Vouchername is required'
        })
    }
    if (!body.discount || body.discount < 0) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Code is required!'
        })
    }
    
    let updateVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    const voucherId = request.params.voucherId;

    //
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'voucherId is invalid'
        })
    }
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}