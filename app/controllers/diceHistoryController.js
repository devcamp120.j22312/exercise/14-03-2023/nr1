const mongoose = require('mongoose');

const diceHistoryModel = require('../models/diceHistoryModel');

const createDiceHistory = (request, response) => {
    let body = request.body;

    if (!body.user) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'User is required!'
        })
    }
    if (!body.dice) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Dice is required!'
        })
    }

    let newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: body.dice,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    diceHistoryModel.create(newDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'New Dice History Has Been Created!',
                data: data
            })
        }
    })
}

const getAllDiceHistory = (request, response) => {
    diceHistoryModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All Dice History',
                data: data
            })
        }
    })
}

const getDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Dice History ID is not valid'
        })
    }

    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get Dice History By Id',
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (request, response) => {
    const diceHistoryId = response.params.diceHistoryId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Dice History ID is not valid'
        })
    }
    if (!body.user) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User is required'
        })
    }
    if (!body.dice) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Dice is required'
        })
    }
    
    let updateDiceHistory = {
        user: body.user,
        dice: body.dice,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deleteDiceHistoryById = (request, response) => {
    const diceHistoryId = request.params.diceHistoryId;

    //
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'DiceHistory ID is invalid'
        })
    }
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}