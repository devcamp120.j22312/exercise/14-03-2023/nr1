const mongoose = require('mongoose');

const userModel = require('../models/userModel');

const createUser = (request, response) => {
    let body = request.body;

    if (!body.username) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Username is required!'
        })
    }
    if (!body.firstname) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Firstname is required!'
        })
    }
    if (!body.lastname) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Lastname is required!'
        })
    }

    let newUser = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }

    userModel.create(newUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'New User Has Been Created!',
                data: data
            })
        }
    })
}

const getAllUser = (request, response) => {
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get All User',
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    const userId = request.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'User ID is not valid'
        })
    }

    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Get User By Id',
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    const userId = response.params.userId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'User ID is not valid'
        })
    }
    if (!body.username) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Username is required'
        })
    }
    if (!body.firstname) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Firstname is required'
        })
    }
    if (!body.lastname) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Lastname is required'
        })
    }
    let updateUser = {
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
        createdAt: body.createdAt,
        updatedAt: body.updatedAt
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Updated!',
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    const userId = request.params.userId;

    //
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'UserId is invalid'
        })
    }
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Successfully Deleted!',
            })
        }
    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}